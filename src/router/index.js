import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/home/home'
import City from '@/pages/city/city'
import Detail from '@/pages/detail/detail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home page',
      component: Home
    },
    {
      path: '/city',
      name: 'city page',
      component: City
    },
    {
      path: '/detail/:id',
      name: 'detail page',
      component: Detail
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return {x: 0, y: 0}
  }
})
