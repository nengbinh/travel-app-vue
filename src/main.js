// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

// in case if lower version andriod phone not supported
import 'babel-polyfill'
// fix each broswer style
import '@style/reset.css'
// fix mobile end pixel issue
import '@style/border.css'
// my customize iconfont
import '@style/iconfont.css'
// fix mobile end 300 millisecond delay
import fastClick from 'fastclick'
// vuex
import store from './store/index'
// use vue-awesome-swiper extension
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
Vue.use(VueAwesomeSwiper)

Vue.config.productionTip = false
fastClick.attach(document.body)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
